```
mvn install

docker run --name gateway-postgres -p 192.168.99.105:5432:5432 -e POSTGRES_PASSWORD=postgres -e PGPASSWORD=postgres -e POSTGRES_USER=postgres -e POSTGRES_DB=postgres -d postgres

docker run -p 6379:6379 -d redis redis-server --requirepass "test123"

docker run -d --name gateway-rabbit -p 5672:5672 rabbitmq
docker run -d --name gateway-rabbit-management -p 15672:15672 rabbitmq:management
```