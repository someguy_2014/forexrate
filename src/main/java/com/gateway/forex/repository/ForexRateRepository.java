package com.gateway.forex.repository;

import com.gateway.forex.model.ForexRate;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ForexRateRepository extends CrudRepository<ForexRate, Long> {

    @Cacheable(cacheNames = "ForexCurrencyByDate", key = "#forexCurrencyId + #currency",
            unless = "#result == null")
    ForexRate findByForexCurrencyIdAndCurrency(Long forexCurrencyId, String currency);

}
