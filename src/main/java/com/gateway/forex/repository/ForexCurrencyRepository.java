package com.gateway.forex.repository;

import java.time.LocalDate;
import java.util.List;
import com.gateway.forex.model.ForexCurrency;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ForexCurrencyRepository extends CrudRepository<ForexCurrency, Long> {

    @Cacheable(cacheNames = "ForexCurrencyByDate", key = "#date + #currency",
            unless = "#result == null")
    ForexCurrency findByDateAndCurrency(LocalDate date, String currency);

    @Cacheable(cacheNames = "ForexCurrencyByPeriod", key = "#dateFrom + #dateTo + #currency",
            unless = "#result == null")
    List<ForexCurrency> findAllByDateLessThanEqualAndDateGreaterThanEqualAndCurrency(
            LocalDate dateFrom, LocalDate dateTo, String currency);
}
