package com.gateway.forex.repository;

import java.util.List;
import com.gateway.forex.model.ForexRequest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ForexRequestRepository extends CrudRepository<ForexRequest, Long> {

    public ForexRequest findByRequestId(String requestId);

    public List<ForexRequest> findAll();

}
