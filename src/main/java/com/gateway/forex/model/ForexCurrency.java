package com.gateway.forex.model;

import java.time.LocalDate;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "forex_currencies",
        uniqueConstraints = @UniqueConstraint(columnNames = {"currency", "date"}))
public class ForexCurrency extends Auditable {

    private static final long serialVersionUID = -8085314081900957599L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "date")
    private LocalDate date;
    @Column(name = "currency")
    private String currency;
    @OneToMany(mappedBy = "forexCurrency", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<ForexRate> forexRates;

}
