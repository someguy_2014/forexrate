package com.gateway.forex.model.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.gateway.forex.model.ForexRequest;
import lombok.Data;

@Data
@XmlRootElement(name = "command")
public class Command implements Serializable {

    private static final long serialVersionUID = -8019636043444539065L;

    @JacksonXmlProperty(isAttribute = true)
    private String id;

    @JacksonXmlProperty(localName = "get")
    private Current current;

    @JacksonXmlProperty(localName = "history")
    private History history;

    public ForexRequest toForexRequest() {
        if ((getCurrent() == null && getHistory() == null)
                || (getCurrent() != null && getHistory() != null))
            throw new RuntimeException();

        ForexRequest forexRequest = new ForexRequest();
        forexRequest.setRequestId(getId());
        if (getCurrent() != null) {
            forexRequest.setClient(getCurrent().getConsumer());
            forexRequest.setCurrency(getCurrent().getCurrency());
        } else {
            forexRequest.setClient(getHistory().getConsumer());
            forexRequest.setCurrency(getHistory().getCurrency());
        }

        return forexRequest;
    }
}


@Data
class Current {

    @JacksonXmlProperty(isAttribute = true)
    private Integer consumer;

    @JacksonXmlProperty(localName = "currency")
    private String currency;
}


@Data
class History {

    @JacksonXmlProperty(isAttribute = true, localName = "consumer")
    private Integer consumer;

    @JacksonXmlProperty(isAttribute = true, localName = "currency")
    private String currency;

    @JacksonXmlProperty(isAttribute = true, localName = "period")
    private int period;
}
