package com.gateway.forex.model.dto;

import java.util.Map;
import lombok.Data;

@Data
public class ForexRateDto {

    private String start_at;
    private String end_at;
    private Map<String, Map<String, Double>> rates;
    private String base;
}
