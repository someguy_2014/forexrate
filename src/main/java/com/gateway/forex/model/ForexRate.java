package com.gateway.forex.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "forex_rates",
        uniqueConstraints = @UniqueConstraint(columnNames = {"forex_currency_id", "currency"}))
public class ForexRate extends Auditable {

    private static final long serialVersionUID = 8485323414915618302L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    @Column
    private String currency;
    @Column
    private Double rate;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "forex_currency_id")
    @JsonIgnore
    private ForexCurrency forexCurrency;


}
