package com.gateway.forex.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "forex_requests")
public class ForexRequest extends Auditable {

    private static final long serialVersionUID = -4355515501286078397L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "request_id", unique = true, length = 255)
    private String requestId;
    @Column(name = "client_id")
    private int client;
    @Column(length = 3)
    private String currency;
    @Column(nullable = true)
    private Long timestamp;
    @Column(nullable = true)
    private Integer period;

}
