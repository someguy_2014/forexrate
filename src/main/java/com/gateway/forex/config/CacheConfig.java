package com.gateway.forex.config;

import java.time.Duration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;

@Configuration
public class CacheConfig {

    @Bean
    public CacheManager cacheManager(RedisConnectionFactory factory) {
        RedisCacheConfiguration config = RedisCacheConfiguration.defaultCacheConfig();
        config = config.entryTtl(Duration.ofMinutes(30)).disableCachingNullValues();

        Set<String> cacheNames = new HashSet<>();
        cacheNames.add("ForexCurrencyByDate");
        cacheNames.add("ForexCurrencyByPeriod");

        Map<String, RedisCacheConfiguration> configMap = new HashMap<>();
        configMap.put("ForexCurrencyByDate", config.entryTtl(Duration.ofMinutes(1)));
        configMap.put("ForexCurrencyByPeriod", config.entryTtl(Duration.ofSeconds(10)));

        RedisCacheManager cacheManager = RedisCacheManager.builder(factory)
                .initialCacheNames(cacheNames).withInitialCacheConfigurations(configMap).build();
        return cacheManager;
    }
}
