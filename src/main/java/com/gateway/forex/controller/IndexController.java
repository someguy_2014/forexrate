package com.gateway.forex.controller;

import java.util.List;
import com.gateway.forex.model.ForexCurrency;
import com.gateway.forex.model.ForexRequest;
import com.gateway.forex.model.dto.Command;
import com.gateway.forex.service.ForexCurrencyService;
import com.gateway.forex.service.ForexRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class IndexController {

    private ForexRequestService forexRequestService;
    private ForexCurrencyService forexCurrencyService;
    private SimpMessagingTemplate messageTemplate;

    @Autowired
    public IndexController(ForexRequestService forexRequestService,
            ForexCurrencyService forexCurrencyService, SimpMessagingTemplate messageTemplate) {
        this.forexRequestService = forexRequestService;
        this.forexCurrencyService = forexCurrencyService;
        this.messageTemplate = messageTemplate;
    }

    @PostMapping(value = "/json_api/current", produces = {MediaType.APPLICATION_JSON_VALUE},
            consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<ForexCurrency> latest(@RequestBody ForexRequest forexRequest,
            BindingResult bindingResult) {
        try {
            if (bindingResult.hasErrors())
                return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);

            ForexRequest record = forexRequestService.addForexRequest(forexRequest);
            if (record == null)
                return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);

            ForexCurrency forexCurrency =
                    forexCurrencyService.findLatestByCurrency(forexRequest.getCurrency());
            return new ResponseEntity<>(forexCurrency, HttpStatus.OK);
        } finally {
            messageTemplate.convertAndSend("/queue/forex-request", forexRequest);
        }
    }

    @PostMapping(value = "/json_api/history", produces = {MediaType.APPLICATION_JSON_VALUE},
            consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<List<ForexCurrency>> history(@RequestBody ForexRequest forexRequest,
            BindingResult bindingResult) {

        try {
            if (bindingResult.hasErrors())
                return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);

            ForexRequest record = forexRequestService.addForexRequest(forexRequest);
            if (record == null)
                return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);

            List<ForexCurrency> result =
                    forexCurrencyService.findHistoryByCurrency(forexRequest.getCurrency(),
                            forexRequest.getTimestamp(), forexRequest.getPeriod());
            return new ResponseEntity<>(result, HttpStatus.OK);
        } finally {
            messageTemplate.convertAndSend("/queue/forex-request", forexRequest);
        }
    }

    @PostMapping(value = "/xml_api/command", produces = {MediaType.APPLICATION_XML_VALUE},
            consumes = {MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<List<ForexCurrency>> command(@RequestBody Command command) {
        ForexRequest forexRequest = command.toForexRequest();

        try {
            ForexRequest record = forexRequestService.addForexRequest(forexRequest);
            if (record == null)
                return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);

            List<ForexCurrency> result =
                    forexCurrencyService.findHistoryByCurrency(forexRequest.getCurrency(),
                            forexRequest.getTimestamp(), forexRequest.getPeriod());
            return new ResponseEntity<>(result, HttpStatus.OK);
        } finally {
            messageTemplate.convertAndSend("/queue/forex-request", forexRequest);
        }
    }
}

