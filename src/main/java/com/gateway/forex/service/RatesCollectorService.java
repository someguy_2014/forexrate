package com.gateway.forex.service;

import java.time.LocalDate;
import java.time.Month;
import java.util.Currency;
import com.gateway.forex.model.ForexCurrency;
import com.gateway.forex.model.dto.ForexRateDto;
import com.gateway.forex.util.DateFormatter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class RatesCollectorService {

    ForexCurrencyService forexCurrencyService;
    ForexRateService forexRateService;

    @Value("${rates.collector.base.url}")
    private String baseUrl;

    public RatesCollectorService(ForexCurrencyService forexCurrencyService,
            ForexRateService forexRateService) {
        this.forexCurrencyService = forexCurrencyService;
        this.forexRateService = forexRateService;
    }

    @Scheduled(fixedRateString = "${rates.collector.fixed.rate.ms}",
            initialDelayString = "${rates.collector.initial.delay.ms}")
    public void load() {
        LocalDate startAt = LocalDate.of(2019, Month.DECEMBER, 31);
        LocalDate endAt = LocalDate.now();

        if (startAt.isEqual(endAt) || startAt.isAfter(endAt))
            return;

        loadRatesFor("history", startAt, endAt, Currency.getInstance("BGN"));
    }

    private void loadRatesFor(String path, LocalDate startAt, LocalDate endAt, Currency currency) {
        RestTemplate restTemplate = new RestTemplate();
        String url = baseUrl + "/" + path;

        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
                    .queryParam("base", currency.getCurrencyCode())
                    .queryParam("start_at", DateFormatter.format(startAt))
                    .queryParam("end_at", DateFormatter.format(endAt));

            ResponseEntity<ForexRateDto> response = restTemplate.exchange(builder.toUriString(),
                    HttpMethod.GET, null, ForexRateDto.class);

            updateRates(response.getBody());
        } catch (Throwable ex) {
            System.err.println(ex.getMessage());
        }
    }

    private void updateRates(ForexRateDto forexRateDto) {
        forexRateDto.getRates().forEach((date, rates) -> {
            ForexCurrency forexCurrency =
                    forexCurrencyService.findOrCreate(forexRateDto.getBase(), date);
            rates.forEach((currency, rate) -> {
                forexRateService.findOrCreate(forexCurrency, currency, rate);
            });
        });
    }
}
