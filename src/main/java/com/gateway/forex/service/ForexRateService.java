package com.gateway.forex.service;

import com.gateway.forex.model.ForexCurrency;
import com.gateway.forex.model.ForexRate;
import com.gateway.forex.repository.ForexRateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ForexRateService {

    private ForexRateRepository forexRateRepository;

    @Autowired
    public ForexRateService(ForexRateRepository forexRateRepository) {
        this.forexRateRepository = forexRateRepository;
    }

    public ForexRate findOrCreate(ForexCurrency forexCurrency, String currency, Double rate) {
        ForexRate forexRate = forexRateRepository
                .findByForexCurrencyIdAndCurrency(forexCurrency.getId(), currency);

        if (forexRate != null)
            return forexRate;

        forexRate = new ForexRate();
        forexRate.setForexCurrency(forexCurrency);
        forexRate.setCurrency(currency);
        forexRate.setRate(rate);

        return forexRateRepository.save(forexRate);
    }

}
