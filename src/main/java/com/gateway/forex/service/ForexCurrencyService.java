package com.gateway.forex.service;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import com.gateway.forex.model.ForexCurrency;
import com.gateway.forex.repository.ForexCurrencyRepository;
import com.gateway.forex.util.DateFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ForexCurrencyService {

    private ForexCurrencyRepository forexCurrencyRepository;

    @Autowired
    public ForexCurrencyService(ForexCurrencyRepository forexCurrencyRepository) {
        this.forexCurrencyRepository = forexCurrencyRepository;
    }

    public ForexCurrency findOrCreate(String currency, String date) {
        ForexCurrency forexCurrency =
                forexCurrencyRepository.findByDateAndCurrency(DateFormatter.format(date), currency);

        if (forexCurrency != null)
            return forexCurrency;

        forexCurrency = new ForexCurrency();
        forexCurrency.setCurrency(currency);
        forexCurrency.setDate(DateFormatter.format(date));

        return forexCurrencyRepository.save(forexCurrency);
    }

    public ForexCurrency findLatestByCurrency(String currency) {
        LocalDate date = LocalDate.now();
        LocalDate friday = date.with(DayOfWeek.FRIDAY);

        if (date.isAfter(friday))
            date = friday;

        return forexCurrencyRepository.findByDateAndCurrency(date, currency);
    }

    public List<ForexCurrency> findHistoryByCurrency(String currency, Long requestDate,
            Integer period) {
        LocalDate dateFrom =
                Instant.ofEpochMilli(requestDate).atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate dateTo = dateFrom.plusDays(period);
        return forexCurrencyRepository.findAllByDateLessThanEqualAndDateGreaterThanEqualAndCurrency(
                dateFrom, dateTo, currency);
    }


}
