package com.gateway.forex.service;

import javax.validation.Valid;
import com.gateway.forex.model.ForexRequest;
import com.gateway.forex.repository.ForexRequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ForexRequestService {

    private ForexRequestRepository forexRequestRepository;

    @Autowired
    public ForexRequestService(ForexRequestRepository forexRequestRepository) {
        this.forexRequestRepository = forexRequestRepository;
    }

    public ForexRequest addForexRequest(@Valid ForexRequest forexRequest) {
        return forexRequestRepository.save(forexRequest);
    }
}
